(function () {
	require('./common/jquery.js');
	require('./common/plupload.full.min.js');
	require('./lib/jquery.tipsy.js');
	require('./complaints/vars.js');
	require('./complaints/ui.js');
	require('./complaints/validate.js');
	require('./complaints/init.js');
}());