complaints.init = function () {
	this.bindUI();
	this.validate();
};

jQuery(document).ready(function($) {
	complaints.init();
});