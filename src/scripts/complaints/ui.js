complaints.bindUI = function () {
	var labels = complaints.labels = {};
	var labelTextToId = {};
	var labelSelected = [];
	var editHidden = $('#description', $form);
	var editableElement = $('.ceditor', $form);
	var labelDesc = $('.label-desc', $form);
	var MAX_LENG = 1000;
	var editorContent;

	/**
	 * 标签操作功能
	 * 点击标签自动切换状态，同时更新描述的内容
	 */
	$labelBox.each(function(index, el) {
		var input = $('input', el)[0],
			id = input.id,
			txt = $.trim( $(el).text() );

		labels[ id ] = txt;
		labelTextToId[txt] = id;
	});

	function textToLabel(text, id) {
		return '<input type="button" disabled class="editor-label" data-id="'+id+'" value="'+ text +'">';
	}
	function labelToText(label) {
		return label.slice(1, -1);
	}
	$body.on('click', '.label-box input', function(e) {
		var label = $(e.target).parent();
		var selected = getContent(editableElement).labels;
		var c = e.target.checked;
		if ( selected.length ) {
			// 新需求只支持一个标签，但是代码是支持多选的
			labelAction( label, 'replace', selected[0] );
		} else {
			labelAction( label, c ? 'replace': 'insert' );
		}
		// return true;
	});
	$body.on('mouseenter', '.label-box', function() {
		$(this).addClass('label-hover');
	}).on('mouseleave', '.label-box', function() {
		$(this).removeClass('label-hover');
	});

	$('.label-box').tipsy({
		gravity: 'n',
		html: true,
		delayIn: 300,
		opacity: 1,
		title: function() {
			return '<h6 class="tipsy-hd">说明详情</h6><p>'+ $(this).data('tip') +'</p>';
		}
	});

	/**
	 * 描述编辑功能
	 * 更新描述内容动态检测标签，并更新相应标签状态
	 * 字数统计
	 * 自定义 placeholder
	 */
	function labelTip(label) {
		if (label) {
			labelDesc.html( $(label).data('tip') );
		} else {
			labelDesc.html('为了更加快捷帮助您解决订单问题，您的问题描述越详细越有助于我们为您解决问题。');
		}
		$('.tipsy').fadeOut('fast');
	}
	// 添加和移除标签
	function labelAction(label, act, replaceId) {
		var $desc = editableElement;
		var v = $desc.html();
		var id = $('input', label)[0].id;
		var labelText = textToLabel( $.trim($(label).text()), id);
		switch(act) {
			case 'insert':
				labelSelected.push( id );
				$desc.focus();
				pasteHtmlAtCaret( labelText );
				$desc.change().trigger('input');
				label.addClass('label-selected');
				labelTip(label);
				break;
			case 'remove':
				labelSelected.splice($.inArray(id, labelSelected), 1);
				$desc.find('[data-id='+id+']').remove();
				$desc.change().trigger('input');
				label.removeClass('label-selected');
				labelTip();
				break;
			case 'replace':
				labelSelected.splice($.inArray(replaceId, labelSelected), 1, id);
				$desc.find('[data-id='+replaceId+']').replaceWith(labelText);
				$desc.change().trigger('input');
				label.addClass('label-selected');
				$('#'+replaceId).removeClass('label-selected');
				labelTip(label);
				break;
		}
		moveCursorToEnd( $desc[0] );
	}
	// 动态检测标签
	function checkLabel(mList) {
		var labelText, labelId;

		$('.label-box').each(function(index, el) {
			$(el).removeClass('label-selected');
			$('input', el).attr('checked', false);
		});

		if (mList) {
			for (var i = mList.length - 1; i >= 0; i--) {
				$( '#'+ mList[i] )
					.attr('checked', true)
					.parent().addClass('label-selected');
			}
		}
	}
	// 把光标定位到末尾
	function moveCursorToEnd(contentEditableElement) {
		var range,selection;
		if(document.createRange) {
			//Firefox, Chrome, Opera, Safari, IE 9+
		    range = document.createRange();//Create a range (a range is a like the selection but invisible)
		    range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
		    range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
		    selection = window.getSelection();//get the selection object (allows you to change selection)
		    selection.removeAllRanges();//remove any selections already made
		    selection.addRange(range);//make the range you have just created the visible selection
		} else if(document.selection) {
			//IE 8 and lower
		    range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
		    range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
		    range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
		    range.select();//Select the range (make it the visible selection
		}
	}
	// 在光标插入内容
	function pasteHtmlAtCaret(html) {
	    var sel, range;
	    if (window.getSelection) {
	        // IE9 and non-IE
	        sel = window.getSelection();
	        if (sel.getRangeAt && sel.rangeCount) {
	            range = sel.getRangeAt(0);
	            range.deleteContents();

	            // Range.createContextualFragment() would be useful here but is
	            // only relatively recently standardized and is not supported in
	            // some browsers (IE9, for one)
	            var el = document.createElement("div");
	            el.innerHTML = html;
	            var frag = document.createDocumentFragment(), node, lastNode;
	            while ( (node = el.firstChild) ) {
	                lastNode = frag.appendChild(node);
	            }
	            range.insertNode(frag);

	            // Preserve the selection
	            if (lastNode) {
	                range = range.cloneRange();
	                range.setStartAfter(lastNode);
	                range.collapse(true);
	                sel.removeAllRanges();
	                sel.addRange(range);
	            }
	        }
	    } else if (document.selection && document.selection.type != "Control") {
	        // IE < 9
	        document.selection.createRange().pasteHTML(html);
	    }
	}

	// 字数统计
	function wordCounter(len) {
		var c = MAX_LENG - +len;
		$('.wordcounter .red').text( c );
		return c;
	}
	// 让编辑区正常化
	function normalize(contenteditable) {
		pasteHtmlAtCaret('<span id="ce-normalize-el"></span>');
		moveCursorToEnd(contenteditable);
		$('#ce-normalize-el',contenteditable).remove();
	}

	function getContent(edit) {
		edit = edit.jquery ? edit[0] : edit;
		var children = edit.childNodes || edit.children;
		var t = '';
		var c = $(edit).text().length || 0;
		var labels = [];
		var ch;
		if (!children.length) {
			t = $(edit).text();
		} else {
			for (var i = 0; i < children.length; i++) {
				ch = children[i];
				if ( ch.value ) {
					if ( /^input$/i.test(ch.nodeName) && $(ch).hasClass('editor-label') ) {
						t += '#' + ch.value + '#';
						labels.push( $(ch).data('id') );
					} else {
						t += ch.value;
					}
				} else {
					t += $(ch).text();
				}
			}
		}
		if ( c === 0 && labels.length === 0 ) {
			normalize(editableElement[0]);
			labelTip();
		}
		return {text: t,count:c, labels: labels};
	}

	$body.on('focus input propertychange keyup', '.ceditor', function(event) {
		var html = $(this).html();
		var isEmpty = html.length;
		editorContent = getContent(this);
		isEmpty ?
			$(this).siblings('.placeholder').hide()
			: $(this).siblings('.placeholder').show();

		wordCounter(editorContent.count);
		editHidden.val(editorContent.text).trigger('input');
		checkLabel(editorContent.labels);
	}).on('blur', '.ceditor', function(event) {
		$(this).html().length <= 0 && $(this).siblings('.placeholder').show();
	});
	$body.on('click', '.placeholder', function(event) {
		$(this).siblings('.ceditor').focus();
	});
	// autofocus 也可防止textarea有默认文本时与自定义placeholder重叠
	editableElement.focus();

	// 支持拖拽
	var dragEl;
	$body.on('dragstart dragend', function(event) {
		if (event.type === 'dragstart') {
			dragEl = event.target;
		} else {
			dragEl = null;
		}
	});
	$body.on('drop', '.ceditor', function(event) {
		event.preventDefault();
		var dragData;
		try {
			dragData = event.originalEvent.dataTransfer.getData('Text/html');
			dragData = dataFilter( dragData );
		} catch(e){
			if (dragEl) {
				dragData = dragEl.innerText ||dragEl.contentText;
			}
		}
		if (dragData) {
			$(event.target).focus();
			pasteHtmlAtCaret( dragData );
			$(event.target).focus();
		}
	});
	function dataFilter(html) {
		return $(html).text();
	}
	// 支持复制
	$body.on('paste', '.ceditor', function(event) {
		event.preventDefault();
		var text = (event.originalEvent.clipboardData ||
			window.clipboardData ||
			event.target.contentWindow.clipboardData ||
			window.dataTransfer).getData('Text');

		event.target.focus();
		pasteHtmlAtCaret( text );
		$(event.target).focus().change();
	});



	/**
	 * 添加投诉列表
	 */
	function addComplaintsLi(data, type) {
		var complaintsList = data && data.complains[0];
		var typeTip = complaintsList && complaintsList.attachments && !complaintsList.content ? '上传凭证' : '提交投诉';
		var _html = ['<li class="complaints-li user-li" style="display:none;">',
						'<div class="img-box">',
							'<img alt="用户头像" src="'+ data.headImgUrl +'" width="48" height="48">',
						'</div>'].join('');

		var msgEl = $('<div></div>');
		var attachments, item;
		if ( complaintsList ) {
			_html += '<div class="text-box">';

			_html += '<p class="time">'+complaintsList.createTime+ '&nbsp;&nbsp;'+ typeTip +'</p>';
			_html += '<div class="dialog-box"><div class="arrow"></div><div class="dialog-inner">';
			_html += '<p>'+ complaintsList.content +'</p>';
			attachments = complaintsList.attachments;
			if (attachments) {
				for (var i = 0, len = attachments.length; i < len; i++) {
					item = attachments[i];
					img = new Image();
					oldHandle = img.onload;
					img.src = item.url;
					link = $('<a class="complaints-img" target="_blank" href="'+ item.url +'" title="'+ item.name +'"></a>')
							.append(img);
					msgEl.append(link);
				}
				_html += msgEl.html();
			}
			_html += '</div></div></div></li>';
		}
		var $list = $('.history-inner .complaints-list');
		var $userLi = $(_html);

		if ( $list.length === 0 ) {
			$('.history-inner').html('<ul class="complaints-list"></ul>');
			$list = $('.history-inner .complaints-list');
		}

		$list.prepend( $userLi.fadeIn() );
	}

	/**
	 * 上传凭证操作
	 */
	var uploader;
	var UPLOADED = [];
	
	// 添加上传凭证缩略图
	function addImageThub(src, fileName) {
		var _html = '<li class="proof-thub" style="display:none;">'+
						'<span class="validator-icon icon-false" title="删除凭证"></span>'+
						'<div class="img-box"><img src="'+proofImageHost+src+'" alt="'+ fileName +'" title="'+fileName+'" /></div>'+
						'<p class="proof-name">'+ fileName +'</p>';
					'</li></ul>';
		var $thubList = $fileRow.find('.thub-list');

		if ( !$thubList.length ) {
			$thubList = $('<ul class="thub-list"></ul>');
			$fileRow.append( $thubList );
		}
		$thubList.append( $(_html).fadeIn() );
	}

	function updateImageData() {
		var $el = $('.thub-list img');
		var str = '';
		UPLOADED = [];

		if ( $el.length ) {
			$el.each(function(index, el) {
				str += ',{"'+ el.src + '":"' +  el.alt + '"}';
				UPLOADED.push( el.src );
			});
			str = '['+ str.slice(1) + ']';
		}
		$imagePath.val(str).trigger('validate');
	}
	// 上传凭证
	function uploadProof() {
		var container = $('#proofContainer');
		uploader = new plupload.Uploader({
			runtimes: 'html4',
			browse_button : 'proof',
			container : container[0],
			url: $uploadProofUrl.val(),
			// flash_swf_url: '/plupload.flash.swf',
			flash_swf_url : '/Moxie.swf',
			total: 2,
			max_file_count: 5,
			max_file_size : '5mb',
			filters: [{
				title: 'Image files',
				extensions: 'jpg,gif,png,jpeg,bmp'
			}],
			headers: {'Accept': '*/*'}
		});
		uploader.bind('init', function() {
			// Plupload 在IE下有个bug，不能正确的获得container的大小
			$('.moxie-shim.moxie-shim-html4').css({
				width: container.width(),
				height: container.height()
			});
		});
		uploader.init();
		uploader.bind('FilesAdded', function(up, args){
			if ( UPLOADED.length > 5 ) {
				for (var i = args.length - 1; i >= 0; i--) {
					uploader.removeFile( args[i] );
				}
				
				alert('一次最多上传5张图片');
				return false;
			} else {
				up.start();
				up.refresh();
			}
		});
		uploader.bind('Error', function(up, args){
			if(args.code == '-600'){
				alert('您上传的头像过大，图片不超过5M');
			}
			up.refresh();
			return false;
		});

		window.uploadCallback = function(a){
			try {
				var data = a.replace(/.+?(\{.+\}).+/g, '$1');
				data = $.parseJSON(data);
			} catch (e) {
				throw Error('数据解析失败:'+data);
			}

			if (data.data) {
				data = data.data[0];
				addImageThub(data.img, data.src_file);
				updateImageData(data.img, data.src_file);
				updateUrl();
				UPLOADED.push( data.src_file );
			} else {
				alert(data.errmsg || '上传失败');
			}
		};
	}
	// 更新上传地址
	function updateUrl() {
		$.get('/complain/getUploadImgUrl.do', function(data) {
			if (data && data.success && data.data) {
				uploader.settings.url = data.data;
			}
		});
	}
	function resizeImage(img, maxWidth, maxHeight) {
		var width = img.width;
		var height = img.height;
		maxWidth = 200 || maxWidth;
		maxHeight = 150 || maxHeight;
		if ( width > height ) {
			if (width > maxWidth) {
				$(img).css({
					width: maxWidth,
					height: maxWidth * height/width
				});
			}
		} else {
			if (height > maxHeight) {
				$(img).css({
					width: maxHeight * width/height,
					height: maxHeight
				});
			}
		}
	}
	uploadProof();
	
	// 删除上传凭证
	$body.on('click', '.proof-thub .icon-false', function() {
		$(this).parent('.proof-thub').hide('fast', function() {
			var img = $('img', this)[0];
			$(this).remove();
			updateImageData();
		});
	});

	/**
	 * form 提交
	 */
	$form.bind('pass.vee', function(event) {
		event.preventDefault();
		submitHandle(this);
		$form.data('pass', false);
	});

	// 表单提交成功
	var $tsTitle = $('.ts-title').first();
	var $newTitle = $(tsTitleTemp);
	var $formShotcut = $toggleBox.clone(); // toggleBox 初始状态的快照
	function formAnim() {
		var par = $tsTitle.parent();
		if ( par.length && !par.is('.result-title') ) {
			$tsTitle.after( $newTitle ).hide('fast', function () {
				$tsTitle.hide(function () {
					$tsTitle.remove();
				});
			});
			$newTitle.slideDown('fast', function () {
				$form.hide();
				resetVar();
			});
		} else {
			$form.slideUp('fast');
			resetVar();
		}
		$(':submit', $form).addClass('btn-gray').attr('disabled', true);
	}
	function formSuccess(data) {
		if (data.status === 0) {
			formAnim();
			addComplaintsLi(data.data, 'user');
		} else if (data.errorMsg) {
			alert('当前订单不属于您账户(账户名)下的订单，请登录其他账户后进行投诉');
		}
	}
	function resetVar() {
		$description = $('.ceditor');
		$wordCounter = $('.wordcounter .red');
		$('.thub-list', $form).remove();
		$imagePath.val('');
		UPLOADED = [];
		$toggleBox = $toggleBox.html( $formShotcut.html() );
	}
	function submitHandle(form) {
		var $this = $(form);
		var param = $(form).serialize();
		var method = form.method;
		var url = form.action;

		$.ajax({
			url: url,
			type: method,
			dataType: 'json',
			data: param,
			success: formSuccess,
			error: function () {
				alert('提交失败');
			}
		});
	}

	/**
	 * 再次投诉
	 */
	$body.on('click', '#complaints-again', function(event) {
		event.preventDefault();
		$form.toggle('fast', function() {
			var arrow = $('> i',event.currentTarget);

			arrow.toggleClass('icon-darrow');
			arrow.toggleClass('icon-uarrow');
		});
	});

	// resize Image
	$('.dialog-inner img', $body).each(function(i, el) {
		if (el.width) {
			resizeImage(el);
		} else {
			$(el).bind('load', function loadHandle() {
				resizeImage(this);
				$(this).unbind('load', loadHandle);
			});
		}

		$('.q_page')[0].style.zoom = 1;
	});

	// 身份验证
	var verifyCode = + $('[name=errCode]', $verifyForm).val();
	var verifyMsg;
	if (verifyCode) {
		verifyMsg = $('[name=errMsg]', $verifyForm).val();
		alert( '当前订单不属于您账户(账户名)下的订单，请登录其他账户后进行投诉' );
	}
};
