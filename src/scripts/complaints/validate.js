complaints.validate = function () {
	var Vee = function () {
		if (!(this instanceof Vee)) {
			return new Vee;
		}
		var nodes = [];
		var pattern = {
			telephone: function () {
				return /^1\d{10}$/.test( this.value );
			},
			no_empty: function () {
				return this.value !== '';
			},
			ceditor: function () {
				return $(this).text() !== '';
			}
		};
		this.nodes = nodes;
		this.pattern = pattern;
	};

	Vee.prototype.verify = function(el, patt, msg) {
		el = el.jquery ? el[0] : el;
		var v = this.pattern[patt].call(el);
		$(this).trigger( v? 'pass.vee': 'fail.vee', [el, patt, msg]);
		return v;
	};
	Vee.prototype.verifyAll = function(cb) {
		var that = this;
		var fails = [];
		var verify = this.verify;
		var nodes = this.nodes;
		$.each(nodes, function(index, node) {
			var v = verify.call(that, node.el, node.patt);
			if (!v) {
				fails.push(node);
			}
		});
		if (cb && typeof cb === 'function') {
			cb(!fails.length, fails);
		}
		return !fails.length;
	};
	Vee.prototype.addNode = function (option) {
		var opt = $.extend({evt: 'change', patt: 'no_empty'}, option);
		var that = this;
		var verify = this.verify;
		this.nodes.push({
			el: opt.el,
			patt: opt.patt
		});
		$(opt.el).bind(opt.evt || 'change', function(event) {
			verify.call(that, opt.el, opt.patt, opt.msg);
		});
	};
	Vee.prototype.addPatt = function (name, fn) {
		return this.pattern[name] = fn;
	};

	// just do it.
	var vee = new Vee();
	vee.addPatt('ceditor', function () {
		return $(this).text() !== '';
	});
	vee.addNode({
		el: $('#contact', $form),
		patt: 'no_empty',
		msg: '请输入正确的联系人姓名'
	});
	vee.addNode({
		el: $('#tel', $form),
		patt: 'telephone',
		msg: '请输入正确的手机号'
	});
	vee.addNode({
		el: $('.ceditor', $form),
		patt: 'ceditor',
		evt: 'input',
		msg: '问题描述为空'
	});
	$(vee).on('pass.vee fail.vee', function(event, el, patt, msg) {
		event.preventDefault();
		if (event.type === 'pass') {
			showErr(el, true);
		} else {
			showErr(el, msg);
		}
	});
	$form.submit(function(event) {
		event.preventDefault();
		vee.verifyAll(function (ret, fails) {
			if (ret) {
				$form.trigger('pass.vee');
			} else {
				$form.trigger('fail.vee');
			}
		});
	});

	function showErr(el, msg) {
		var $el = $(el);
		var icon = msg === true ? true : false;
		var $tip = $('<div class="validator-msg"></div>');
		var pos = $el.position();
		var w = $el.outerWidth(true);
		var h = $el.outerHeight(true);
		var _html = '';

		msg = msg || $el.data('msg') || '';
		$el.next('.validator-msg').remove();

		$tip.css({
			left: pos.left + w+1,
			top: pos.top + 4
		});

		if ( icon ) {
			_html = $tip.html( '<i class="validator-icon icon-true"></i>' );
			$el.after(_html);
		} else {
			_html = ['<i class="validator-icon icon-false"></i>',
						'<div class="validator-inner">',
						msg,
						'</div>'].join('');

			$tip.html( _html );
			$el.after($tip);
		}
	}
};
