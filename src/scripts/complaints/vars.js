var $body = $('body.complaints-page');
var $wordCounter = $('.wordcounter .red');
var $description = $('#description');
var $proof = $('#proof');
var $labelBox = $('.label-box');
var $form = $('#complaints-form', $body);
var $fileRow = $('.file-row', $form);
var $verifyForm = $('#complaints-verify-form', $body);
var $uploadProofUrl = $('[name="uploadImgUrl"]');
var $imagePath = $('[name="imageJson"]', $form);
var $toggleBox = $('#complaints-form .toggle-box');
var tsTitleTemp = ['<div class="ts-title clearfix result-title">',
				'<h3 class="fL"><i class="red">*</i><b>亲爱的用户，您已成功提交投诉或凭证，去哪儿网将竭诚为您解决问题，请等待回电。</b></h3>',
				'<a href="#" class="fR" id="complaints-again">再次投诉<i class="icon-darrow"></i></a>',
			'</div>'].join('');
var proofImageHost = 'http://img1.qunarzz.com';
var complaints = {};